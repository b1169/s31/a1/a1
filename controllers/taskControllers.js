const Task = require("./../models/Tasks");

module.exports.createTask = (reqBody) => {
	let newTask = new Task({
		name: reqBody.name
	})

	return newTask.save().then(result => result)
}

module.exports.getAllTasks = (reqBody) =>{
	return Task.find().then((result=>{
		return result}
		))
}

module.exports.updateTask = (reqBody) => {

	return Task.findOneAndUpdate({name: reqBody}, {status: "complete"}).then(result => result)
}

module.exports.updateTaskbyID = (reqParams) => {

	return Task.findByIdAndUpdate({_id: reqParams.id}, {status: "complete"}).then(result => result);
}

module.exports.getById = (params) =>{

	return Task.findById({_id: params.id}).then(result => result)
}