const express = require("express");
const router = express.Router();

const taskController = require("./../controllers/taskControllers.js")

router.post("/", (req, res) => {

	taskController.createTask(req.body).then(result => res.send(result))
})

router.get("/alltasks", (req, res) =>{

	taskController.getAllTasks(req.body).then(result => res.send(result))
})

router.put("/change-status", (req, res) => {

	taskController.updateTask(req.body.name).then(result => res.send(result))
})

router.put("/:id/complete", (req,res) => {
	taskController.updateTaskbyID(req.params).then(result => res.send(result))
})

router.get("/:id", (req, res) => {

	taskController.getById(req.params).then(result => res.send(result))
})


module.exports = router;